# Comandos básicos para gerenciamento do switch core Dell S3124
Neste documento serão apresentados comandos básicos para executar atividades diárias.

> A evolução em como tabalhar nos equipamentos sempre será constate. O próprio site da Dell (suporte e fórum) é um ótimo local para busca de conhecimento e tirar dúvdas.

> **ATENÇÂO!!** Este equipamento é literalmente o `coração da rede`. Qualquer alteração incorreta podeŕa comprometer **TODA** a rede da KFG Distribuidora. Lembre-se, ele possui garantida Dell Pró-Support, caso existe alguma dificuldade/problema não exite em entrar em contato o suporte Dell.

## Conectando no equipamento
Existem duas maneiras fáceis de acessar o dispositivo, por cabo console ou por SSH.

Para acesso SSH, deve-se utilizar o usuário e senha de **admin** e poderá ser feito através do PuTTY ou qualquer outro cliente SSH.

> O switch core não possui acesso web, somente terminal. Seu IP é `192.168.0.254`, porém qualquer IP de VLAN poderá ser usado.

### Estou conectado, e agora?
Agora precisamos identificar aonde estamos dentro do switch. Existem 2 modos de execução de comandos:
- Privilégio
    - Aqui pode-se apenas executar tarefas básicas e listar informações do sistema. Para identificar, ao logar, o campo de execução de comandos será desta forma: `swtichS3124#`
- Configuração
    - Nível de privilégio máximo, aqui poderá entrar em todas as configurações. Para identificar, ao logar, o campo de execução de comandos será desta forma: `(config)#`
    
### Comandos básicos - Listando toda a configuração
No modo de privilégio, podemos verificar todas as configurações atuais executando o comento `show running-config`.
A saída do comando será algo parecido com:
````
swtichS3124# show running-config
Current Configuration ...
! Version 9.11(2.0)
! Last configuration change at Mon Dec  3 18:33:59 2018 by admin
! Startup-config last updated at Mon Dec  3 18:34:05 2018 by admin
!
boot system stack-unit 1 primary system://A
boot system stack-unit 1 secondary system://B
boot system stack-unit 2 primary system://A
boot system stack-unit 2 secondary system://B
!
logging coredump stack-unit  1 
logging coredump stack-unit  2 
logging coredump stack-unit  3 
logging coredump stack-unit  4 
logging coredump stack-unit  5 
logging coredump stack-unit  6 
logging coredump stack-unit  7 
logging coredump stack-unit  8 
logging coredump stack-unit  9 
logging coredump stack-unit  10 
logging coredump stack-unit  11 
logging coredump stack-unit  12 
!
hostname swtichS3124
!
protocol lldp 
!
redundancy auto-synchronize full
...
````
> Para poder continuar a rolagem das configurações, aperte a barra de espaços.

> É possível executar comandos de forma abreviada, o comando `show running-config` pode ser executado como `show run`

## Alguns comandos úteis de privilégio
### Falando um pouco sobre VLANs
Uma rede local virtual, normalmente denominada de VLAN, é uma rede logicamente independente. Várias VLANs podem coexistir em um mesmo computador (switch), de forma a dividir uma rede local (física) em mais de uma rede (virtual), criando domínios de broadcast separados. Uma VLAN também torna possível colocar em um mesmo domínio de broadcast, hosts com localizações físicas distintas e ligados a switches diferentes. Um outro propósito de uma rede virtual é restringir acesso a recursos de rede através de recursos de camada 3.

Redes virtuais operam na camada 2 do modelo OSI. No entanto, uma VLAN geralmente é configurada para mapear diretamente uma rede ou sub-rede IP, onde entre um switch camada 3.

#### Um pouco mais de teoria
Qual a diferença entre equipamentos de camada 2 e camada 3?

O modelo OSI diz que a camada 2 é a camada de transporte e a camada 3 é a camada de rede.

Tentando ser mais simples, equipamentos de camada 3 "*conversam*" entre sí através de IPs (rede). Chama-se essa conversa de **pacotes**.

Já equipamentos camada 2 "*conversam*" entre sí através do *Mac Address*, ao invés de ter a informação de IP. Essa conversa tem o nome de **quadro** (ou também frame).

Interessante ter isso em mente até para entender como funciona a rede. Em switches de acesso (camada 2), somente será informado a VLAN que a porta irá trafegar já no swtich core (camada 3), serão aplicados controle de acessos, criar a VLAN e colocar um IP, onde é possível verificar a diferença entre eles.

O *Tag* de uma VLAN sempre ocorrerá na camada 2. Por isso um switch de acesso não precisa saber IP da VLAN, a conversa entre ele (acesso) e o camada 3 (core) ocorrerá sempre por **frames** .

 



### Verificar todas as VLANs
Comando irá trazer todos IDs de VLAN, Nome e IP das mesmas.
````
swtichS3124#show vlan brief
VLAN Name                             STG   MAC Aging IP Address
---- -------------------------------- ----  --------- ------------------
1                                     0     1800      unassigned
2    TI                               0     1800      192.168.2.254/24
3    CFTV                             0     1800      192.168.3.254/24
4    TV                               0     1800      192.168.4.254/24
5    Controle de Acesso               0     1800      192.168.5.254/24
6    Wireless Corporativo             0     1800      192.168.6.254/24
7    Wireless Funcionarios            0     1800      192.168.7.254/24
8    Wireless Visitantes              0     1800      192.168.8.254/24
9    Impressora                       0     1800      192.168.9.254/24
10   Recepcao                         0     1800      192.168.10.254/24
11   Tele Vendas                      0     1800      192.168.11.254/24
12   Sala Reuniao                     0     1800      192.168.12.254/24
13   SAC                              0     1800      192.168.13.254/24
14   RH                               0     1800      192.168.14.254/24
15   Diretoria                        0     1800      192.168.15.254/24
16   Administracao                    0     1800      192.168.16.254/24
17   Credito Cobranca                 0     1800      192.168.17.254/24
18   Marketing                        0     1800      192.168.18.254/24
19   Finan Contabil                   0     1800      192.168.19.254/24
20   Expedicao                        0     1800      192.168.20.254/24
21   Guarita                          0     1800      192.168.21.254/24
22   eCommerce                        0     1800      192.168.22.254/24
100  Servidores                       0     1800      192.168.0.254/24
101  Gerencia Switches                0     1800      192.168.1.254/24
````
### Verificar informações de uma única VLAN
Aqui podemos verificar em quais locais uma VLAN específica está a associada:
````
swtichS3124#show vlan id 2

Codes: * - Default VLAN, G - GVRP VLANs, R - Remote Port Mirroring VLANs, P - Primary, C - Community, I - Isolated
       O - Openflow, Vx - Vxlan
Q: U - Untagged, T - Tagged
   x - Dot1x untagged, X - Dot1x tagged
   o - OpenFlow untagged, O - OpenFlow tagged
   G - GVRP tagged, M - Vlan-stack
   i - Internal untagged, I - Internal tagged, v - VLT untagged, V - VLT tagged

    NUM    Status    Description                     Q Ports
    2      Active    TI                              T Po1(Gi 1/14,Gi 2/14)
                                                     T Po2(Gi 1/15,Gi 2/15)
                                                     T Po3(Gi 1/16,Gi 2/16)
                                                     T Po4(Gi 1/17,Gi 2/17)
                                                     T Po5(Gi 1/18,Gi 2/18)
                                                     T Po6(Gi 1/19,Gi 2/19)
                                                     T Po7(Gi 1/20,Gi 2/20)
                                                     T Po8(Gi 1/21,Gi 2/21)
                                                     T Po9(Gi 1/22,Gi 2/22)
````
Entendendo as informações:
- **NUM**
    - É o ID da VLAN;
- **Status**
    - Aqui pode-se verificar se ela está funcional ou não. Neste caso está tudo OK;
    > Toda VLAN recém criada sempre irá aparecer inativa até ser associada a um destino
- **Description**
    - É o nome literal da VLAN;
- **Q**
    - Aqui é a VLAN está configurada para a porta (ao lado);
- **Ports**
    - Qual porta é referente ao caompo **Q**;
        - Todas as portas sempre aparecerão por linhas.
        
>**NOTA 1:** Existe duas definições de tipos de configuração de porta, **trunk** e **acesso** (ou access).
Portas em modo **acesso** são somente para uma VLAN. É aonde um computador está conectado. Portas em modo **trunk**
são portas de comunição entre  switches e nelas, eles irão entender quais VLANs estão configuradas entre eles.

>**NOTA 2:** A configuração **Untagged** é equivalente a porta em modo acesso. A configuração **Tagged** é equivalente a porta em moto trunk.
Faça uma associação do **T** (Tagged = Trunk)!!

### Verificando o status físico da porta
Este comando mostra algumas informações da interface física:
````
swtichS3124#show interfaces status
Port                 Description  Status Speed        Duplex Vlan
Gi 1/1               Firebox_em0  Up     1000 Mbit    Full   --
Gi 1/2               Server_1-Po  Up     1000 Mbit    Full   --
Gi 1/3               FILE_SERVER  Up     1000 Mbit    Full   --
Gi 1/4               SERVER_2-en  Up     1000 Mbit    Full   --
Gi 1/5                            Down   Auto         Auto   100
Gi 1/6                            Down   Auto         Auto   100
Gi 1/7               Firewall_fi  Down   Auto         Auto   100
Gi 1/8                            Down   Auto         Auto   100
Gi 1/9                            Down   Auto         Auto   100
Gi 1/10                           Down   Auto         Auto   100
Gi 1/11              CENTRAL_TEL  Up     100 Mbit     Full   100
Gi 1/12              SERVER_3     Up     1000 Mbit    Full   100
Gi 1/13                           Down   Auto         Auto   100
Gi 1/14              SWA_gi24     Up     1000 Mbit    Full   --
Gi 1/15              SWB_gi24     Up     1000 Mbit    Full   --
Gi 1/16              SWC_gi24     Up     1000 Mbit    Full   --
Gi 1/17              SWD_gi24     Up     1000 Mbit    Full   --
Gi 1/18              SWE_gi24     Up     1000 Mbit    Full   --
Gi 1/19              SWF_gi24     Up     1000 Mbit    Full   --
Gi 1/20              SW23_24_gi2  Up     1000 Mbit    Full   --
Gi 1/21              SWCA_g24     Up     1000 Mbit    Full   --
Gi 1/22              SWCB_g24     Up     1000 Mbit    Full   --
Gi 1/23                           Down   Auto         Auto   1
Gi 1/24                           Down   Auto         Auto   1
Te 1/25              SWCD_Galpao  Up     1000 Mbit    Full   3,6-8,20,22,100-101
Te 1/26              SWCC_Guarit  Up     1000 Mbit    Full   3,5-8,21,100-101
Te 1/27                           Down   Auto         Auto   --
Te 1/28                           Down   Auto         Auto   --
Gi 2/1               Firebox_em5  Up     1000 Mbit    Full   --
Gi 2/2               Server_1-Po  Up     1000 Mbit    Full   --
Gi 2/3               FILE_SERVER  Up     1000 Mbit    Full   --
Gi 2/4               SERVER_2-en  Up     1000 Mbit    Full   --
Gi 2/5                            Down   Auto         Auto   100
Gi 2/6                            Down   Auto         Auto   100
Gi 2/7                            Down   Auto         Auto   100
Gi 2/8                            Down   Auto         Auto   100
Gi 2/9                            Down   Auto         Auto   100
Gi 2/10                           Down   Auto         Auto   100
Gi 2/11                           Down   Auto         Auto   100
Gi 2/12                           Down   Auto         Auto   100
Gi 2/13                           Down   Auto         Auto   100
Gi 2/14                           Up     1000 Mbit    Full   --
Gi 2/15              SWB_gi23     Up     1000 Mbit    Full   --
Gi 2/16              SWC_gi23     Up     1000 Mbit    Full   --
Gi 2/17              SWD_gi23     Up     1000 Mbit    Full   --
Gi 2/18              SWE_gi23     Up     1000 Mbit    Full   --
Gi 2/19              SWF_gi23     Up     1000 Mbit    Full   --
Gi 2/20              SW23_24_gi2  Up     1000 Mbit    Full   --
Gi 2/21              SWCA_g23     Up     1000 Mbit    Full   --
Gi 2/22              SWCB_g23     Up     1000 Mbit    Full   --
Gi 2/23                           Down   Auto         Auto   --
Gi 2/24                           Down   Auto         Auto   --
Te 2/25                           Down   Auto         Auto   --
Te 2/26                           Down   Auto         Auto   --
Te 2/27                           Down   Auto         Auto   --
Te 2/28                           Down   Auto         Auto   --
````    
Entendo as informações:
- **Port**
    - Nome *físico* da interface;
- **Descrição**
    - Descrição para melhor entender aonde a porta está conectada;
- **Status**
    - Mostra se a porta está ativa ou não;
- **Speed**
    - Qual é a velocidade que a porta está funcionando;
- **Duplex**
    - Qual a negociação dq porta;
- **Vlan**
    - Se houver VLAN sendo Taggeada na porta física, ela irá aparecer aqui também. 
    
### Verificar como está a configuração física da porta
Util para saber qual VLAN está na porta.

````
swtichS3124#show interfaces switchport gi2/10

Codes:  U - Untagged, T - Tagged
        x - Dot1x untagged, X - Dot1x tagged
        G - GVRP tagged, M - Trunk
        i - Internal untagged, I - Internal tagged, v - VLT untagged, V - VLT tagged

Name: GigabitEthernet 2/10
802.1QTagged: Hybrid
Vlan membership:
Q       Vlans
U       100

Native VlanId:    100.
````                

### Adicionando uma porta a uma VLAN
Agora será necessário entrar no modo de configuração.
> Neste exemplo será simulado um para mostrar o uso do comando acima

````
swtichS3124#configure terminal
swtichS3124(conf)#interface vlan 2
swtichS3124(conf-if-vl-2)#show config
!
interface Vlan 2
 description TI
 name TI
 ip address 192.168.2.254/24
 tagged Port-channel 1-9
 ip helper-address 192.168.0.1
 no shutdown
swtichS3124(conf-if-vl-2)#untagged gi 2/10
% Error: Gi 2/10 Port is untagged in another Vlan.
````
No exemplo acima, foi tentado adicionar a porta Gi2/10 na VLAN 2 e houve o retorno informando que a mesma já está em outra VLAN.

Neste caso, com o comando para verificar a configuração da porta física (`show interfaces switchport gi2/10`), pode ser visto qual a VLAN a porta faz parte e remove-la.

> **NOTA:** Verifique que no escopo de configuração, foi utilizado o comando `show config`, este comando retorna como está
a configuração de comandos corrigos. Este comando poderá ser utilizado praticamente todo o lugar.  

#### Criar VLAN
````
swtichS3124#configure terminal
swtichS3124(conf)#interface vlan 22
swtichS3124(conf-if-vl-22)#description Teste
swtichS3124(conf-if-vl-22)#name Teste
swtichS3124(conf-if-vl-22)#ip address 1.2.3.4 255.255.255.0
swtichS3124(conf-if-vl-22)#no shutdown
swtichS3124(conf-if-vl-22)#show config
!
interface Vlan 2
 description TI
 name TI
 ip address 192.168.2.254/24
 no shutdown
swtichS3124(conf-if-vl-22)#end
swtichS3124#
````
> Toda interface (VLAN, Port-Channel, Gigabit..) vem com seu estado default como **shutdown**. É preciso passar o comando
`no shutdown` para fazer com que ela consigar ficar ativa.

> Pelo fato de cada VLAN criada ser um domínio broadcast, é necessário informar para VLAN quem será o **ip helper-address**. Basicamente, este é o único IP que a VLAN deixará
passar broadcast, mesmo não fazendo parte da rede da VLAN. Isso sempre será necessário para aplicações de servidores DHC, por exemplo.

#### Adicionar porta como acesso a uma VLAN
````
swtichS3124#configure terminal
swtichS3124(conf)#interface vlan 22
swtichS3124(conf-if-vl-22)#untagged gi 1/1
swtichS3124(conf-if-vl-22)#end
swtichS3124#
swtichS3124#show interfaces switchport gi1/1

Codes:  U - Untagged, T - Tagged
        x - Dot1x untagged, X - Dot1x tagged
        G - GVRP tagged, M - Trunk
        i - Internal untagged, I - Internal tagged, v - VLT untagged, V - VLT tagged

Name: GigabitEthernet 1/1
802.1QTagged: Hybrid
Vlan membership:
Q       Vlans
U       22

Native VlanId:    22.
````

#### Remover porta como acesso a uma VLAN
````
swtichS3124#configure terminal
swtichS3124(conf)#interface vlan 22
swtichS3124(conf-if-vl-22)#no untagged gi 1/1
swtichS3124(conf-if-vl-22)#end
swtichS3124#
````

#### Adicionar uma porta como trunk 
````
swtichS3124#configure terminal
swtichS3124(conf)#interface vlan 22
swtichS3124(conf-if-vl-22)#tagged gi 1/1
swtichS3124(conf-if-vl-22)#end
swtichS3124#
swtichS3124#show interfaces switchport gi1/1

Codes:  U - Untagged, T - Tagged
        x - Dot1x untagged, X - Dot1x tagged
        G - GVRP tagged, M - Trunk
        i - Internal untagged, I - Internal tagged, v - VLT untagged, V - VLT tagged

Name: GigabitEthernet 1/1
802.1QTagged: Hybrid
Vlan membership:
Q       Vlans
T       22

Native VlanId:    22.
````

#### Remover porta como trunk
````
swtichS3124#configure terminal
swtichS3124(conf)#interface vlan 22
swtichS3124(conf-if-vl-22)#no tagged gi 1/1
swtichS3124(conf-if-vl-22)#end
swtichS3124#
````

### Trabalhanco com ACLs
**A**ccess **C**ontrol **L**ists ou, em bom Português: listas de controle de acesso. Aqui aonde é feito todo o controle de acesso da **LAN**, liberando ou negando redes ou liberando serviços específicos  de uma rede para outro.

Trabalhar com ACL pode ser muito complexo, como muito simples. **TODA** a atenção é realmente necessaria ao criar ou alterar qualquer ACL.

>Qualquer alteração mal feita poderá literalmente parar a rede da KFG Distribuidora!!

> **Lembre-se:** Restrições de navegação para internet não deve ser feito no switch (ACL). ACLs devem ser feitas para controle de acesso entre as VLANs. Sempre libere o acesso ao firewall para que ele faça o controle de navegação.

No equipamento, todas as ACLs são apliacas para a VLAN. Usaremos a VLAN e ACL do Administrativo:

VLAN:
````
swtichS3124# show running-config interface vlan 16
interface Vlan 16
 description Administracao
 name Administracao
 ip address 192.168.16.254/24
 tagged Port-channel 1-9
 ip access-group ACL_ADM in
 ip helper-address 192.168.0.1
 no shutdown
````

Sabendo o nome da ACL, agora vamos ver como ela está configurada:
````
swtichS3124#show ip access-lists ACL_ADM in
Extended Ingress IP access list ACL_ADM
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 permit tcp any 192.168.15.0/24 eq 3389
 seq 81 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.17.0/24
 seq 100 deny ip any 192.168.18.0/24
 seq 110 deny ip any 192.168.19.0/24
 seq 120 deny ip any 192.168.20.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
 ````
 
 Para atribuir uma ACL a uma VLAN, observe na configuração da mesma existe o comanto `ip access-group ACL_ADM in`.
 Este comando irá atribuir a ACL para a VLAN no trafego de **entrada**.
 
 > Está sendo tratado a ACL como **in** por ser mais fácil saber que alguém irá "entrar" na VLAN para consumir, por exemplo um destino **HTTP**. 

#### Editando uma ACL
Primeiramente, vamos entender o que é cara campo desta linha `seq 10 deny ip any 192.168.1.0/24`.

> ACL sempre será aplicada conforme a sequência de cada linha que ela foi criada. Se a primeira linha negar todo o trafeo,
as demais linhas nao terão efeito de lieração.

- **seq 10**
    - É a sequencia númerica. Não existe uma padrão de criação porém é interessante que sejam criadas como decimal (10, 20)
    pois caso haja necessidade de uma regra intermediária, poderá ser criada com valores unitários (11, 12,13...);
- **deny**
    - Neste caso é uma ação de negação. A ação de liberação é *permit*;
- **ip**
    - Quando informado, todas as portas estão inclusas. Podera também ser **tcp** e **udp** porém será preciso informar
    as portas que será aplicada (vide seq 80);
- **any**
    - Informa que a origem é qualquer IP/rede
- **192.168.1.0/24**
    - Informa que o destino é a rede 192.168.1.0/24.
    
A leitura da regra na ***seq 10***, é que será negado toda a origem para o destino 192.168.1.0/24.     
   