#!/bin/bash
sw_core="192.168.0.254"
sw_access="1.1.1.1 2.2.2.2"
Backup_Server=`/sbin/ifconfig eth0 | grep "inet addr:" | awk '{print $2}'  | awk -F\: '{print $2}'`
Ftp_Dest="/Backup"
BACKUPDIR="/mnt/backup/RouterSwitchs/Dell/" #Definir route ou switch
User="devices.backup"
Pass="senha"
Backup_Server=`/sbin/ifconfig eth0 | grep "inet addr:" | awk '{print $2}'  | awk -F\: '{print $2}'`
Ftp_Dest=`grep ^$User /etc/passwd | awk -F\: '{print $6}'` #devices.backup:x:503:503::/Backup:/sbin/nologin
BACKUPDIR="/mnt/backup/RouterSwitchs/Cisco/" #No caso, está montado o cifs, mas dependo pode ser pasta local
LOGALLOWED="20"
MAILTO="miguel.longo@betha.com.br"
LOGALLOWED="20"

## !! Se usar ssh, instalar o ssh

##
# Deve-se criar um usuário com privilégio baixo para poder logar e fazer
# o backup das confs de todos os equipamentos. Exemplo cisco:
## Cisco Configuration
# conf t
# username devices.backup privilege 0 password 0 bth.1234
# privilege exec level 0 copy startup-config ftp:192.168.2.150
# ip ftp username devices.backup
# ip ftp password bth.1234
# exit
#
# Caso for roteador de filial troca o privilege por
# privilege exec level 0 copy startup-config ftp:187.59.87.174

# SCP Keyauth
# conf t
# (config)#ip scp server enable
# (config)#ip ssh pubkey-chain
# (conf-ssh-pubkey)#username pipi
# (conf-ssh-pubkey-user)#key-string
# (conf-ssh-pubkey-data)#key from id_rsa.pub
# (conf-ssh-pubkey-data)#exit

# Start Backup
ERRMSG=""
ERRSUM=0
DATESCRIPT=`date +%F_%H_%M_%S

Core(){
        for asa in $ASAS
        do
                ping -c 1 $asa &> /dev/null
                if [ $? == 0 ]
                then
                        (sleep 2; echo "ena"; echo $Pass ;echo "copy startup-config ftp://devices.backup:bth.1234@192.168.2.150/${asa}_startup-config"; sleep 2; echo -en \\n; sleep 2; echo -en \\n; sleep 2; echo -en \\n; sleep 2; echo -en \\n; sleep 15 ;echo "exit") | sshpass -p senha  ssh -T -t $User@$asa
                    
                        #Verifica se Arquivo foi copiado, se ele existe em /Backup
                        ls -l $Ftp_Dest/${asa}_startup-config  &> /dev/null
                        if [ $? -ne 0 ]
                        then
                                ERRSUM=1
                                if [ "$ERRMSG" == "" ]
                                then
                                        ERRMSG="$asa"
                                else
                                        ERRMSG="$ERRMSG, $asa"
                                fi
                        fi
                        else
                        ERRSUM=1
                        if [ "$ERRMSG" == "" ]
                        then
                                ERRMSG="Host Inacessivel $asa"
                        else
                                ERRMSG="$ERRMSG, Host Inacessivel $asa"
                        fi
                fi
        done
}

funCisco(){
        for cisco in $CISCOS
        do
                ping -c 1 $cisco &> /dev/null
                if [ $? == 0 ]
                then
                        (sleep 2;echo $User; sleep 2;echo $Pass; sleep 2 ; echo "ena"; echo $Pass ;echo "copy startup-config ftp:"; sleep 2;echo $Backup_Server; sleep 2;echo "${cisco}_startup-config"; sleep 5;echo "exit") | telnet $cisco
                        
                        #Verifica se Arquivo foi copiado, se ele existe em /Backup
                        ls -l $Ftp_Dest/${cisco}_startup-config  &> /dev/null
                        if [ $? -ne 0 ]
                        then
                                ERRSUM=1
                                if [ "$ERRMSG" == "" ]
                                then
                                        ERRMSG="$cisco"
                                else
                                        ERRMSG="$ERRMSG, $cisco"
                                fi
                        fi
                        else
                        ERRSUM=1
                        if [ "$ERRMSG" == "" ]
                        then
                                ERRMSG="Host Inacessivel $cisco"
                        else
                                ERRMSG="$ERRMSG, Host Inacessivel $cisco"
                        fi
                fi
        done
}

#-- FINAL --
funFim(){
        # Zip'em all
        cd "$Ftp_Dest"
        tar -jcf Dell_Backup.${DATESCRIPT}.tbz *
        mv Dell_Backup.${DATESCRIPT}.tbz $BACKUPDIR/

        if [ "`pwd`" == "$Ftp_Dest" ]
        then
                rm -f *
        fi

        # Notify if error
        source /var/scripts/dell2800/variavel_dell
        if [ $ERRSUM == 1 ]  || [ $DELLERRSUM == 1 ]
        then
                echo -en Switches DELL with errors: $ERRMSG\\n\\n ---- $DELLERRMSG\\n\\n Old backups were not deleted\\n
                echo -en $ERRMSG \\n $DELLERRMSG | /opt/sendEmail-v1.55/sendEmail -f alerta.ti@bethasistemas.com.br -xu alerta.ti -xp asdfgh -t paulo.duzzioni@betha.com.br -u "Backup Switch DELL" -s 192.168.5.80:25
                exit 1
        fi

#       # Backup Rotation
        LOGS=`ls ${BACKUPDIR}/Dell_Backup* | wc -l`
        if [ $LOGS -gt $LOGALLOWED ]
        then
                logs_2_delete=`expr $LOGS - $LOGALLOWED`
                ls ${BACKUPDIR}/Dell_Backup* -t | tail -n $logs_2_delete | while read bp2del
                do
                        rm -f "${bp2del}"
                done
        fi
} 

FTP(){
        # FTP Devices Cisco
        if netstat -an |grep :21; then
                #echo "Serviço OK"
                echo -en Servico FTP OK!\\n\\n
                return 0
        else
                echo -en Serviço FTP parado. Subindo serviço.\\n\\n
                /etc/init.d/proftpd start
                return 1
        fi

} 

TFTP(){
        # FTP Devices Cisco
        if netstat -an |grep :69; then
                #echo "Serviço OK"
                echo -en Servico TFTP OK!\\n\\n
                return 0
        else
                echo -en Serviço TFTP parado. Subindo serviço.\\n\\n
                service xinetd start
                return 1
        fi

} 

CIFS(){
        if grep "/mnt/backup" /proc/mounts; then
                #funCisco "$CISCOS"
                echo -en CIFS ok!\\n\\n
                return 0
        else
                echo -en CIFS not found!\\n
                mount /mnt/backup/
                return 1
        fi
} 

comeca(){
        FTP
        if [ $? -ne 0 ]; then
                echo -en WARNING, return 0 from function FTP\\nTry again.\\n\\n
                FTP
        fi

        TFTP
        if [ $? -ne 0 ]; then
                echo -en WARNING, return 0 from function TFTP\\nTry again.\\n\\n
                TFTP
        fi

        CIFS
        if [ $? -ne 0 ]; then
                echo -en WARNING, return 0 from function CIFS\\nTry again.\\n\\n
                CIFS
        fi

        backupCore
        funFim
} 

# Inicia Backup
comeca
