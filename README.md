# KFG Distribuidora - Projeto Network  
  
Aqui está documentado de forma descritiva, toda a alteração/evolução que foi efetuada em toda a rede da KFG Distribuidora, visando garantir disponibilidade e melhoria de desempenho.  
  
>**NOTA:** Todas as informações aqui contidas poderão ser alteradas/atualizadas caso necessário. A mesma está em repositório Gitlab para tal.  

### [Instruções do switch Core](sw-core.md)


### [Instruções do switch de Acesso](sw-access.md)
  
## LAN - Como estava, objetivo  
A rede da KFG Distribuidora, inicialmente não havia segmentação. Toda a rede era feita através de switchs L2, sem gerenciamento e o firewall da Ostec como único domínio broadcast.  
  
`LAN: 192.168.0.0/24 Default GW: 192.168.0.1 (firewall)`  
  
Devido a necessidade de melhorar desempenho, gerenciamento e segurança, iniciou um projeto de construção de data center onde neste projeto contemplava a reestruturação da rede,  conforme esboço


```mermaid
graph TD;
    CORE-->SW-A;
    CORE-->SW-B;
	CORE-->SW-C;
	CORE-->SW-D;
	CORE-->SERVER-A
...
```

## Instalação/conexão física dos dispositivos

 - Switch Core
	 - Foram instalados dois switchs Dell da linha S3100 (S3124 - 28-port GE/TE) em topo de rack. Este foram "stackeados" conforme documentação apresentada pela Dell, usando a service tag do equipamento;
	 - Foi configurado o usuário ***admin*** com a senha fornecida;
	 - Foi configurado as VLANs conforme reunião (tabela abaixo);
	 - Foi configurado serviços como NTP, SNMP, dns, SSH entre outros para melhor gestão do equipamento;
	 - Foram conectados e configurados todos os switchs de acesso em LACP. Considere os Dell N1524 e os Intelbras que já haviam na rede (tabela abaixo);
	 - Foram conectados e configurados todos os servidores em LACP, exceto o *SERVER-3* por não haver placa de rede.

 - Switches de acesso
    - Todas as portas foram configuradas e testadas conforme levantamento apresentando;
    - Foi configurado o usuário ***admin*** com a senha fornecida;
    - Foi configurado serviços como NTP, SNMP, dns, SSH entre outros para melhor gestão do equipamento;
    - Todos os dispositivos foram atualizados para a ultima versão de firmware presente no momento da instalaçao;
    - Foi apresentando todas as VLANs para todos os switches de acesso;
    - Todos switches (Dell e Intelbras) foram configurados com LCAP.

### Tabela de VLANs

| ID da VLAN			| Setor				| IP				| Mascara			| ACL		|
|-----------------------|-------------------|-------------------|-------------------|-----------|
|1	| *Sem uso*			| *Sem uso*			| *Sem uso*			| *Sem uso*	
|2	| TI				| 192.168.2.254		| 255.255.255.255.0 | Não possui
|3	| CFTV				| 192.168.3.254		| 255.255.255.255.0	| Não possui
|4	| TV				| 192.168.4.254		| 255.255.255.255.0	| ACL_TV
|5	| Controle de Acesso | 192.168.5.254	| 255.255.255.255.0 | Não possui
|6	| Wireless Corporativo | 192.168.6.254  | 255.255.255.255.0 | Não possui
|7	| Wireless Funcionarios | 192.168.7.254	| 255.255.255.255.0 | ACL_WIRELESS_FUNCIONARIOS
|8	| Wireless Visitantes	| 192.168.8.254	| 255.255.255.255.0 | ACL_WIRELESS_VISITANTES
|9	| Impressora			| 192.168.9.254	| 255.255.255.255.0 | Não possui
|10	| Recepcao				| 192.168.10.254 | 255.255.255.255.0 | ACL_RECEPCAO
|11	| Tele Vendas	| 192.168.11.254 | 255.255.255.255.0 | ACL_TELEVENDAS
|12	| Sala Reunião	| 192.168.12.254 | 255.255.255.255.0 | Não possui
|13 | SAC	| 192.168.13.254 | 255.255.255.255.0 | ACL_SAC
|14	| RH	| 192.168.14.254 | 255.255.255.0 | ACL_RH
|15	| Diretoria	| 192.168.15.254 | 255.255.255.255.0 | Não possui
|16 | Administração | 192.168.16.254 | 255.255.255.255.0 | ACL_ADM
|17 | Credito Cobranca | 192.168.17.254 | 255.255.255.255.0 | ACL_CRED_COBRANCA
|18 | Marketing | 192.168.18.254 | 255.255.255.255.0 | ACL_MARKETING
|19 | Finan Contabil | 192.168.19.254 | 255.255.255.255.0 | ACL_FINAN_CONTABIL
|20 | Expedicao | 192.168.20.254 | 255.255.255.255.0 | ACL_EXPEDICAO
|21 | Guarita | 192.168.21.254 | 255.255.255.255.0 | ACL_GUARITA
|22 | eCommerce | 192.168.22.254 | 255.255.255.255.0 | ACL_ECOMMERCE
|100 | Servidores | 192.168.0.254 | 255.255.255.255.0 | Não possui
|101 | Gerencia Switches | 192.168.1.254 | 255.255.255.255.0 | ACL_GERENCIA_SW
> **NOTA 1:** O IP da VLAN sempre será o *default gateway* da rede que ela representa.

> **NOTA 2:** VLANs que possuem a informação no campo ACL como "*Não possui*", indica que não está sendo aplicado nenhum controle de acesso, sendo liberado todo o trafego de entreada para a mesma

> **NOTA 3:** A **VLAN 1** está marcado como "*Sem uso"* por ser reservado pelo switch. 

### Tabela de IPs dos Switchs

| IP    | Nome  |
|-------|-------|
|192.168.1.254 | CORE
|192.168.1.253 | SWA
|192.168.1.252 | SWB
|192.168.1.251 | SWC
|192.168.1.250 | SWD
|192.168.1.249 | SWE
|192.168.1.249 | SWF
|192.168.1.248 | SW23_24
|192.168.1.247 | SWCA
|192.168.1.246 | SWCB
|192.168.1.245 | SWCD
|192.168.1.244 | SWCC

### Tabela de portas LCAP - CORE
|  | Portas SW | Destino| Port-Channel |
|----|-----------|--------|------------|
|1 | Gi1/1 - Gi2/1 | Firewall | Port-Channel 10
|2 | Gi1/2 - Gi2/2 | SERVER-1 | Port-Channel 11
|3 | Gi1/3 - Gi2/3 | FILE_SERVER |  Port-Channel 13
|4 | Gi1/4 - Gi2/4 | SERVER_2 | Port-Channel 12
|5 | Gi1/14 - Gi2/14 | SWA | Port-Channel 1
|6 | Gi1/15 - Gi2/15 | SWB | Port-Channel 2
|7 | Gi1/16 - Gi2/16 | SWC | Port-Channel 3
|8 | Gi1/17 - Gi2/17 | SWD | Port-Channel 4
|9 | Gi1/18 - Gi2/18 | SWE | Port-Channel 5
|10 | Gi1/19 - Gi2/19 | SWF | Port-Channel 6
|11 | Gi1/20 - Gi2/20 | SW23_24 | Port-Channel 7
|12 | Gi1/21 - Gi2/21 | SWCA | Port-Channel 8
|13 | Gi1/22 - Gi2/22 | SWCB | Port-Channel 9
> **NOTA:** Está representado aqui somente as portas em LCAP, demais portas não estão apresentandas

### ACLs
Aqui estão as ACEs e ACLs que foram criadas para melhorar a segurança da rede KFG:
> ACLs estão todas corridas, não será explicada cada ACL. 
````
ip access-list extended ACL_ADM
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 permit tcp any 192.168.15.0/24 eq 3389
 seq 81 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.17.0/24
 seq 100 deny ip any 192.168.18.0/24
 seq 110 deny ip any 192.168.19.0/24
 seq 120 deny ip any 192.168.20.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_CRED_COBRANCA
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 permit tcp any 192.168.15.0/24 eq 3389
 seq 81 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.16.0/24
 seq 100 deny ip any 192.168.18.0/24
 seq 110 deny ip any 192.168.19.0/24
 seq 120 deny ip any 192.168.20.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_ECOMMERCE
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 permit tcp any 192.168.15.0/24 eq 3389
 seq 81 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.16.0/24
 seq 100 deny ip any 192.168.17.0/24
 seq 110 deny ip any 192.168.18.0/24
 seq 120 deny ip any 192.168.19.0/24
 seq 130 deny ip any 192.168.20.0/24
 seq 140 deny ip any 192.168.21.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_EXPEDICAO
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 permit tcp any 192.168.15.0/24 eq 3389
 seq 81 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.16.0/24
 seq 100 deny ip any 192.168.17.0/24
 seq 110 deny ip any 192.168.18.0/24
 seq 120 deny ip any 192.168.19.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_FINAN_CONTABIL
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 permit tcp any 192.168.15.0/24 eq 3389
 seq 81 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.16.0/24
 seq 100 deny ip any 192.168.17.0/24
 seq 110 deny ip any 192.168.18.0/24
 seq 120 deny ip any 192.168.20.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_GERENCIA_SW
 seq 1 permit ip any 192.168.2.0/24
 seq 10 deny ip any 192.168.3.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.5.0/24
 seq 40 deny ip any 192.168.6.0/24
 seq 50 deny ip any 192.168.7.0/24
 seq 60 deny ip any 192.168.8.0/24
 seq 70 deny ip any 192.168.9.0/24
 seq 80 deny ip any 192.168.10.0/24
 seq 90 deny ip any 192.168.11.0/24
 seq 100 deny ip any 192.168.12.0/24
 seq 110 deny ip any 192.168.13.0/24
 seq 120 deny ip any 192.168.14.0/24
 seq 130 deny ip any 192.168.15.0/24
 seq 140 deny ip any 192.168.16.0/24
 seq 150 deny ip any 192.168.17.0/24
 seq 160 deny ip any 192.168.18.0/24
 seq 170 deny ip any 192.168.19.0/24
 seq 180 deny ip any 192.168.20.0/24
 seq 190 deny ip any 192.168.21.0/24
 seq 200 deny ip any 192.168.22.0/24
!
ip access-list extended ACL_GUARITA
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.16.0/24
 seq 100 deny ip any 192.168.17.0/24
 seq 110 deny ip any 192.168.18.0/24
 seq 120 deny ip any 192.168.19.0/24
 seq 130 deny ip any 192.168.20.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_MARKETING
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 deny ip any 192.168.14.0/24
 seq 80 permit tcp any 192.168.15.0/24 eq 3389
 seq 81 deny ip any 192.168.15.0/24
 seq 90 deny ip any 192.168.16.0/24
 seq 100 deny ip any 192.168.17.0/24
 seq 110 deny ip any 192.168.19.0/24
 seq 120 deny ip any 192.168.20.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_RECEPCAO
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.11.0/24
 seq 70 deny ip any 192.168.13.0/24
 seq 80 deny ip any 192.168.14.0/24
 seq 90 permit tcp any 192.168.15.0/24 eq 3389
 seq 91 deny ip any 192.168.15.0/24
 seq 100 deny ip any 192.168.16.0/24
 seq 110 deny ip any 192.168.17.0/24
 seq 120 deny ip any 192.168.18.0/24
 seq 130 deny ip any 192.168.19.0/24
 seq 140 deny ip any 192.168.20.0/24
 seq 150 deny ip any 192.168.21.0/24
 seq 160 deny ip any 192.168.22.0/24
 seq 170 permit ip any any
!
ip access-list extended ACL_RH
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.13.0/24
 seq 70 permit tcp any 192.168.15.0/24 eq 3389
 seq 71 deny ip any 192.168.15.0/24
 seq 80 deny ip any 192.168.16.0/24
 seq 90 deny ip any 192.168.17.0/24
 seq 100 deny ip any 192.168.18.0/24
 seq 110 deny ip any 192.168.19.0/24
 seq 120 deny ip any 192.168.20.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_SAC
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 60 deny ip any 192.168.14.0/24
 seq 70 permit tcp any 192.168.15.0/24 eq 3389
 seq 71 deny ip any 192.168.15.0/24
 seq 80 deny ip any 192.168.16.0/24
 seq 90 deny ip any 192.168.17.0/24
 seq 100 deny ip any 192.168.18.0/24
 seq 110 deny ip any 192.168.19.0/24
 seq 120 deny ip any 192.168.20.0/24
 seq 130 deny ip any 192.168.21.0/24
 seq 140 deny ip any 192.168.22.0/24
 seq 150 permit ip any any
!
ip access-list extended ACL_TELEVENDAS
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.4.0/24
 seq 30 deny ip any 192.168.7.0/24
 seq 40 deny ip any 192.168.8.0/24
 seq 50 deny ip any 192.168.10.0/24
 seq 70 deny ip any 192.168.13.0/24
 seq 80 deny ip any 192.168.14.0/24
 seq 90 permit tcp any 192.168.15.0/24 eq 3389
 seq 91 deny ip any 192.168.15.0/24
 seq 100 deny ip any 192.168.16.0/24
 seq 110 deny ip any 192.168.17.0/24
 seq 120 deny ip any 192.168.18.0/24
 seq 130 deny ip any 192.168.19.0/24
 seq 140 deny ip any 192.168.20.0/24
 seq 150 deny ip any 192.168.21.0/24
 seq 160 deny ip any 192.168.22.0/24
 seq 170 permit ip any any
!
ip access-list extended ACL_TV
 seq 10 deny ip any 192.168.1.0/24
 seq 20 deny ip any 192.168.3.0/24
 seq 30 deny ip any 192.168.4.0/24
 seq 40 deny ip any 192.168.5.0/24
 seq 50 deny ip any 192.168.6.0/24
 seq 60 deny ip any 192.168.7.0/24
 seq 70 deny ip any 192.168.8.0/24
 seq 80 deny ip any 192.168.9.0/24
 seq 90 deny ip any 192.168.10.0/24
 seq 100 deny ip any 192.168.11.0/24
 seq 110 deny ip any 192.168.12.0/24
 seq 120 deny ip any 192.168.13.0/24
 seq 130 deny ip any 192.168.14.0/24
 seq 140 deny ip any 192.168.15.0/24
 seq 150 deny ip any 192.168.16.0/24
 seq 160 deny ip any 192.168.17.0/24
 seq 170 deny ip any 192.168.18.0/24
 seq 180 deny ip any 192.168.19.0/24
 seq 190 deny ip any 192.168.20.0/24
 seq 200 deny ip any 192.168.21.0/24
 seq 210 deny ip any 192.168.22.0/24
 seq 220 permit ip any any
!
ip access-list extended ACL_WIRELESS_FUNCIONARIOS
 seq 10 permit ip any host 192.168.0.1
 seq 20 deny ip any 192.168.0.0/24
 seq 30 deny ip any 192.168.1.0/24
 seq 40 deny ip any 192.168.3.0/24
 seq 50 deny ip any 192.168.4.0/24
 seq 60 deny ip any 192.168.5.0/24
 seq 70 deny ip any 192.168.6.0/24
 seq 80 deny ip any 192.168.7.0/24
 seq 90 deny ip any 192.168.8.0/24
 seq 100 deny ip any 192.168.9.0/24
 seq 110 deny ip any 192.168.10.0/24
 seq 120 deny ip any 192.168.11.0/24
 seq 130 deny ip any 192.168.12.0/24
 seq 140 deny ip any 192.168.13.0/24
 seq 150 deny ip any 192.168.14.0/24
 seq 160 deny ip any 192.168.15.0/24
 seq 170 deny ip any 192.168.16.0/24
 seq 180 deny ip any 192.168.17.0/24
 seq 190 deny ip any 192.168.18.0/24
 seq 200 deny ip any 192.168.19.0/24
 seq 210 deny ip any 192.168.20.0/24
 seq 220 deny ip any 192.168.21.0/24
 seq 230 deny ip any 192.168.22.0/24
 seq 240 permit ip any any
!
ip access-list extended ACL_WIRELESS_VISITANTES
 seq 10 permit ip any host 192.168.0.1
 seq 11 deny tcp any host 192.168.0.250 eq 3389
 seq 12 deny tcp any host 192.168.0.250 range 137 139
 seq 13 deny tcp any host 192.168.0.250 eq 445
 seq 14 deny tcp any host 192.168.0.250 range 1000 1999
 seq 15 deny tcp any host 192.168.0.250 eq 1433
 seq 16 deny tcp any host 192.168.0.250 eq 5555
 seq 17 deny tcp any host 192.168.0.250 range 7890 7899
 seq 18 deny tcp any host 192.168.0.250 eq 8443
 seq 19 permit ip any host 192.168.0.250
 seq 20 deny ip any 192.168.0.0/24
 seq 30 deny ip any 192.168.1.0/24
 seq 40 deny ip any 192.168.3.0/24
 seq 50 deny ip any 192.168.4.0/24
 seq 60 deny ip any 192.168.5.0/24
 seq 70 deny ip any 192.168.6.0/24
 seq 80 deny ip any 192.168.7.0/24
 seq 90 deny ip any 192.168.8.0/24
 seq 100 deny ip any 192.168.9.0/24
 seq 110 deny ip any 192.168.10.0/24
 seq 120 deny ip any 192.168.11.0/24
 seq 130 deny ip any 192.168.12.0/24
 seq 140 deny ip any 192.168.13.0/24
 seq 150 deny ip any 192.168.14.0/24
 seq 160 deny ip any 192.168.15.0/24
 seq 170 deny ip any 192.168.16.0/24
 seq 180 deny ip any 192.168.17.0/24
 seq 190 deny ip any 192.168.18.0/24
 seq 200 deny ip any 192.168.19.0/24
 seq 210 deny ip any 192.168.20.0/24
 seq 220 deny ip any 192.168.21.0/24
 seq 230 deny ip any 192.168.22.0/24
 seq 240 permit ip any any

````

## Considerações
Foram apresentadas de forma simples as alterações que foram efetuadas na rede da KFG Distribuidora para implementação do Core L3 com switches de acesso L2 gerenciáveis.

Atualmente a rede encontra-se totalmente redundante, mitigando riscos pontuais, diminuindo consideravelmente os tráfegos broadcast e melhorando todo o desempenho devido a configuração de LACP entre os switches e os principais servidores.

Toda configuração dos escopos DHCP continuaram no firewall, conforme solicitado e configurações pontuais foram feitas *in-loco*, observando necessidades de ajustes manuais nos computadores.

Esperamos assim que todas as atividades tenham ficado a contento do que foi solicitado, e ficamos a disposição para quaisquer necessidades.